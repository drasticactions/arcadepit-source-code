# README #

The source code to the ArcadePit game-show hosted by Smite on twitch.  To help with functionality please check to make sure we need it prior to adding it to the code.  We are currently using HTML5, JS, and JQuery to handle all the layouts and features.



### Contribution guidelines ###

Please TEST your code!!! Use consistent naming convention!!!
Here's a list of what is needed, currently just a copy and paste from Smite.
Smite: So here's all the elements:

1) Gameboard .. doing this on webcam, don't need to worry about that

2) Spinner. Got this one mostly sorted already. Will be on screen with the game board.

3) Quiz. Eight questions that are either 10,20,30 or 40 points. Need those randomly pulled from the list and made so repeat questions can't happen ever unless I want them to. --I got most of the question and answer part finished need the question list--

4) game challenges. Need those randomly pulled from the list. It's okay if the same challenge shows up again, unlike the quiz. It just need to not be in the same pool twice at once. It's be okay if the same game shows up twice, as long as the challenge for it is a different one. I have 2 images of a TV, one with a lit and one with unlit screen. I want to make it so that the website shows the lit TV and game name in there and plays a ding when the website loads. Providing the TVs are in the right positions, I can do the rest in OBS.

5) Drawing challenges. Need to pull randomly and just like the quiz one I won't use the same one again. You can just export the pulled result to a spoiler text thing. It will be one at a time, so maybe a button to generate a new one on the page too?

6) Screenshot challenges. I got a folder locally that has a bunch of images in them with filenames like this "Game Name -- Artist.png" These need to be randomly embedded into an html (into a box with size limits of 500x500). The name and artist could be placed below in spoiler tags again. I won't use the same one twice here either.


shoot me and email if you're interested: sylphidewings@gmail.com